//
//  Pg1.swift
//  Industry Project
//
//  Created by Richard Lu on 16/8/17.
//  Copyright © 2017 Richard Lu. All rights reserved.
//

import UIKit


class Pg1Controller: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.backgroundColor = .green
        
        // Do any additional setup after loading the view, typically from a nib.
    }
}
