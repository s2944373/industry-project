//
//  ViewController.swift
//  Industry Project
//
//  Created by Richard Lu on 16/8/17.
//  Copyright © 2017 Richard Lu. All rights reserved.
//

import UIKit

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    let cellId = "cellId"
    var images: [String] = ["btn_read", "btn_readMyself", "exitbtn_small"]
    
    let backgroundImageView : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named:"firewatch")
        iv.contentMode = .scaleAspectFill
        iv.layer.masksToBounds = true
        return iv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.backgroundView = backgroundImageView
        
        collectionView?.register(TaskButton.self, forCellWithReuseIdentifier: cellId)
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //CELL STUFF
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3 //number of cells
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TaskButton
        cell.button.tag = indexPath.item
        cell.button.image = UIImage(named: images[indexPath.item])
        cell.button.contentMode = .scaleAspectFit
        cell.homeController = self
        return cell
    }
    
    /*
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)//make a cell go all the way across the screen
    }*/
    
    func nextPage(){
        let secondViewController:Pg1Controller = Pg1Controller(collectionViewLayout: UICollectionViewFlowLayout())
        
        self.present(secondViewController, animated: true, completion: nil)
    }
}

class TaskButton: UICollectionViewCell{
    var homeController: HomeController? //provide access to home controller within task button
    
    let button : UIImageView = {
        let l = UIImageView()
        l.translatesAutoresizingMaskIntoConstraints = false //needed to layout to be functional
        l.isUserInteractionEnabled = true
        return l
    }()
    
    override init (frame:CGRect){
        //this func called every time a cell is dequeued
        super.init(frame:frame)
        setupViews()
    }
    
    func handleTap() {
        if (self.button.tag == 2){
            exit(0)
        }
        else{
            print(self.button.tag)
            homeController?.nextPage()
        }
    }
    
    func setupViews(){
        addSubview(button)
        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        
        addContraintsWithFormat(format: "H:|[v0]|", views: button)
        addContraintsWithFormat(format: "V:|[v0]|", views: button)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension UIColor{
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor{
        return UIColor(colorLiteralRed: Float(r/255), green: Float(g/255), blue: Float(b/255), alpha: 1)
    }
}

extension UIView{//make constraints more readable
    func addContraintsWithFormat(format: String, views: UIView...){
        var viewsDictionary = [String: UIView]()
        for(index,view) in views.enumerated(){
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false // needed for addconstraints
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
        
    }
}
